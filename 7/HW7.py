# Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:
#
# Попросіть користувача ввести свсвій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"
#
# Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача
# (1 - рік, 22 - роки, 35 - років і тд...).
# Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "Незважаючи на те, що вам 42 роки, білетів всеодно нема!"
#
# Зробіть все за допомогою функцій! Не забувайте що кожна функція має виконувати
# тільки одне завдання і про правила написання коду.

userinput = input('Введіть будь ласка ваш вік (ціле число) ')

def less_than_seven(arg1, arg2):
    rslt = f'Тобі ж {arg1} {arg2}! Де твої батьки?'
    return rslt

def less_than_sixteen(arg1, arg2):
    rslt = f'Тобі лише {arg1} {arg2}, а це є фільм для дорослих!'
    return rslt

def more_than_sixtyfive(arg1, arg2):
    rslt = f'Вам {arg1} {arg2}? Покажіть пенсійне посвідчення!'
    return rslt

def include_seven(arg1, arg2):
    rslt = f'Вам {arg1} {arg2}, вам пощастить'
    return rslt

def another_age(arg1, arg2):
    rslt = f'Незважаючи на те, що вам {arg1} {arg2}, білетів всеодно нема!'
    return rslt
def wrong_age(arg1):
    rslt = f'{arg1} - невірне значення'
    return rslt

def right_year(arg1):
    if 11 <= arg1 <= 14:
        year = 'років'
    elif arg1 == 1 or arg1 % 10 == 1 and arg1 != 111:
        year = 'рік'
    elif 2 <= arg1 <= 4 or arg1 % 10 == 2 or arg1 % 10 == 3 or arg1 % 10 == 4:
        year = 'роки'
    else:
        year = 'років'
    return year

if userinput.isdigit():
    age = int(userinput)
    year1 = right_year(age)
    if age < 1:
        res = wrong_age(age)
    elif age < 7:
        res = less_than_seven(age, year1)
    elif 6 < age < 16:
        res = less_than_sixteen(age, year1)
    elif age % 10 == 7:
        res = include_seven(age, year1)
    elif age > 65:
        res = more_than_sixtyfive(age, year1)
    else:
        res = another_age(age, year1)

else:
    res = wrong_age(userinput)
print(res)
