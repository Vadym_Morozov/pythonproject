# вивести список всіх астронавтів, що перебувають в даний момент на орбіті
# (дані не фейкові, оновлюються в режимі реального часу)

import requests
from pprint import pprint

url = 'http://api.open-notify.org/astros.json'
response = requests.get(url)
response_json = response.json()
people_one = response_json['people']
people_on_orbit = []
for i in people_one:
    people_on_orbit.append(i['name'])
pprint(people_on_orbit)

# апі погоди (всі токени я для вас вже прописав)
# https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric
# де city_name - назва міста на аглійській мові (наприклад, odesa, kyiv, lviv)
#  погода змінюється, як і місто. яке ви введете
# роздрукувати тепрературу та швидкість вітру. з вказівкою міста, яке було вибране

url = 'https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric'
city = input('Enter city name (for example odesa, kyiv or lviv): ', )
response = requests.get(url.format(city_name = city))
response_json = response.json()
main_temp = response_json['main']['temp']
wind_speed = response_json['wind']['speed']

print(f'In {city}, temperature is {main_temp} and wind speed is {wind_speed}')
