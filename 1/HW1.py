first = 10
second = 30

print(first + second)
print(first - second)
print(first * second)
print(first / second)
print(first % second)
print(first // second)
print(first ** second)

var1 = first > second
var2 = first < second
var3 = first == second
var4 = first != second

print(var1)
print(var2)
print(var3)
print(var4)