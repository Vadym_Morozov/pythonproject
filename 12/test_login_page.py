"""
 https://ziko.od.ua/
 XPATH:
 1. //*[@id='header_links']/li[1]/a
 2. //*[@title="Отопление"]
 3. //*[@id="header_links"]/li [last()]
 4. //div[@class="shopping_cart"]/a
 5. //a [@class="login"]
 6. //button [@class = "btn btn-default button-search"]
 7. //*[@id="first-currencies"]
 8. //*[@class="logo img-responsive"]/parent::*
 9. //*[@class=" a-niveau1" and @data-id = "6"]
 10. //li [@class="li-niveau1 advtm_menu_1 sub"]/a
 11. //*[@class="advtm_menu_span advtm_menu_span_2"]/ancestor::a
 12. //a[@data-id-product="148"]
 13. //li[@class="ajax_block_product col-xs-12 col-sm-4 col-md-3 last-item-of-mobile-line"]//a[@class="product-name"]
 14. //img[@class="replace-2x img-responsive" and contains (@title, 'MSZ')] / ..
 15. //*[contains (@class, 'col-md-3 last-item-of-ta')]//a[@class = "button lnk_view btn btn-default"]
 16. //*[contains (@class, 'blockcategories')]
 17. //*[contains (@title, 'Прохлада')]
 18. //ul[@class="tree dynamized"]/li[2]/a
 19. //*[@class="icon-envelope-alt"]/..//a
 20. //*[@class="footer-block col-xs-12 col-sm-4"]/h4/a
 21. //*[@title="Мои заказы"]
 22. //a[@href="https://ziko.od.ua/order-slip"]
 23. //a[contains(text(), 'адрес')]
 24. //i[@class="icon-phone"]//../span
 25. //*[contains (@href, 'ident')]
 26. //*[@id="block_various_links_footer"]//li[last()-2]//a
 27. //i[@class="icon-map-marker"]/parent::*
 28. //a[text() = 'Internet Studio']
 29. //a[@data-toggle="tab"]
 30. //*[@class="col-xs-6"]

 CSS:
 1. [alt = 'Ziko']
 2. #searchbox
 3. div.shopping_cart>a
 4. a.login
 5. form[id$= 'cy']
 6. [title="Кондиционирование"]
 7. [data-id-product="148"]
 8. [data-id-product="148"]~a
 9. [class="grower CLOSE"]~a[href$="ciya"]
 10. [class="icon-envelope-alt"]~span a
 11. ul.bullet li a[href$="history"]
 12. div.col-xs-6
 13. li.li-niveau1.advtm_menu_4.menuHaveNoMobileSubMenu > a
 14. div.contacts_top
 15. [data-id="163"]
 """

from selenium.webdriver import Chrome
import time

from selenium.webdriver.common.by import By


def test_login_page():
    driver_chrome = Chrome('chromedriver')
    driver_chrome.maximize_window()
    driver_chrome.get('https://ziko.od.ua/authentication')

    login = 'vadymmorozov@ukr.net'
    password = '1q2w3e4r5t'
    email_input_css_locator = '#email'
    email_input_element = driver_chrome.find_element(By.CSS_SELECTOR, email_input_css_locator)
    email_input_element.clear()
    email_input_element.send_keys(login)

    password_input_css_locator = '#passwd'
    passwort_input_element = driver_chrome.find_element(By.CSS_SELECTOR, password_input_css_locator)
    passwort_input_element.clear()
    passwort_input_element.send_keys(password)
    time.sleep(2)

    login_button_css_locator = '#SubmitLogin'
    login_button_element = driver_chrome.find_element(By.CSS_SELECTOR, login_button_css_locator)
    login_button_element.click()
    time.sleep(2)

    driver_chrome.quit()