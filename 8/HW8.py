
def change_to_string(func):
    """Decorator makes str from functions result"""
    def wrapper(*args):
        res2 = str(func(*args))
        return res2
    return wrapper

@change_to_string
def my_func(*args):
    rslt = []
    for i in args:
        rslt.append(i)
    return rslt

res = my_func('qw', 1, 2, 'qwe', '12', 1.09)

print(res, type(res))




