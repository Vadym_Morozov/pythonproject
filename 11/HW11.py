from abc import ABC, abstractmethod
from faker import Faker
from random import randint

fake = Faker()


class SchoolStaff(ABC):
    def __init__(self, name, salary):
        """Initialize self"""
        self.name = name
        self.salary = salary

    @abstractmethod
    def __str__(self):
        pass


class Teacher(SchoolStaff):
    """Adding teachers to school staff"""

    def __str__(self):
        return f' I\'m teacher {self.name} and my salary is {self.salary}'


class Technical(SchoolStaff):
    """Adding teachnicals to school staff"""

    def __str__(self):
        return f' I\'m technical {self.name} and my salary is {self.salary}'


class School:

    def __init__(self, name: str, school_superintendent: Teacher, number_of_teachers: int = 20, number_of_technicals: int = 10):
        """Initialize school name, school superintendent, list of teacher and technical staff"""
        self.name = name
        self.school_superintendent = school_superintendent
        self.list_of_teachers = [Teacher(fake.name(), randint(10000, 25000)) for position in range(number_of_teachers)]
        self.list_of_technicals = [Technical(fake.name(), randint(5000, 10000)) for position in range(number_of_technicals)]

    @property
    def get_total_salary(self):
        """Count total salary"""
        all_staff = []
        all_staff.append(self.school_superintendent)
        all_staff += self.list_of_teachers
        all_staff += self.list_of_technicals
        total = sum((obj.salary for obj in all_staff))
        return total

    def change_school_superintendent(self):
        """Change school superintendent, add old one to the teachers list and remove new school superintendent from teaachers list"""
        old_superintendent = self.school_superintendent
        new_candidate = self.list_of_teachers.pop()
        self.school_superintendent = new_candidate
        self.list_of_teachers.append(old_superintendent)


school106 = School('School 106', Teacher('Anton Pavlov', 50000))
school106.list_of_teachers.append(Teacher('Kiril Antonov', 15000))
school106.change_school_superintendent()
all_teachers = school106.list_of_teachers
total_salary = school106.get_total_salary
