# Дана довільна строка.
# Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.

text = input('Введіть будьласка текст українською мовою:', )
golosni = ['а', 'е', 'є', 'и', 'і', 'ї', 'о', 'у', 'ю', 'я']
text = text.lower().split()
words = 0

for word in text:
    counter = 0
    for letter in word:
        if letter in golosni:
            counter += 1
        else:
            counter = 0
        if counter == 2:
            words += 1
            break

print('Кількість слів з подвійною голосною:', words)

# Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами:
# Напишіть код, який знайде і виведе на екран назви магазинів,
# ціни яких попадають в діапазон між мінімальною і максимальною ціною.
# Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"

lower_limit = float(input('Введіть будьласка мінімальну ціну:', ))
upper_limit = float(input('Введіть будьласка максимальну ціну:', ))
lst = []
if lower_limit <= 0 or upper_limit <= 0:
    print('Ціна повинна бути більше за 0')
if lower_limit >= upper_limit:
    print('Мінімальна ціна повинна бути меньше за максимальну')
my_dict = { "cito": 47.999,
            "BB_studio": 42.999,
            "momo": 49.999,
            "main-service": 37.245,
            "buy.now": 38.324,
            "x-store": 37.166,
            "the_partner": 38.988,
            "sota": 37.720,
            "rozetka": 38.003}
for key, value in my_dict.items():
    if upper_limit > value > lower_limit:
        lst.append(key)
print(lst)
