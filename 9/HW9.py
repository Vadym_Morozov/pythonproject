def is_number_even(arg):
    '''
    Function check is received number is even or not
    :param arg:
    :type arg: int
    :return: True | False
    '''
    check = arg % 2 == 0
    return check


even_checker = is_number_even(56)
even_checker_1 = is_number_even(33)
print(even_checker)
print(even_checker_1)

assert is_number_even(56) is True
assert type(even_checker) == bool



def capitalize_check(arg):
    '''
    Function received string and checking is string belong to method capitalize or not
    :param arg:
    :type arg: str
    :return: True | False
    '''
    result = arg.capitalize()
    return result == arg


capitalize_checker = capitalize_check("")
print(capitalize_checker)

capitalize_checker1 = capitalize_check("sadvasv")
print(capitalize_checker1)

assert type(capitalize_checker) == bool
assert capitalize_check('') is True
assert capitalize_check('My name is David') is False
assert capitalize_check('') is True




def decorator(some_text_message):
    '''
    Decorator add message
    :param some_text_message:
    :return:
    '''
    def add_print(func):
        '''
        Decorator add print to result of another function
        :param func:
        :return:
        '''
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            print(result, some_text_message)
            return result
        return wrapper
    return add_print


@decorator("Here is only str type")
def text_only(*args, **kwargs):
    '''
    Function choose only str type and add it to new list
    :param args:
    :return:
    '''
    only_text = []
    for i in args:
        if isinstance(i, str):
            only_text.append(i)
    for k, v in kwargs:
        if isinstance(v, str):
            only_text.append(v)
    return only_text


a = text_only('qw', 1, 2, "sac", 'qwe', '12', 1.09)
