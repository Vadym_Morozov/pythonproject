import datetime
import uuid

class BankAccaunt:
    deposit_bag: int = 0

    def __init__(self, name, summ, percentage_charge):
        """Initialize self"""
        self.name = name
        self.first_deposit = summ
        self.__percentage_charge = percentage_charge
        self.__open_date = datetime.date.today()
        self.accaunt_id = uuid.uuid4()
        BankAccaunt.accounting(summ, income=True)

    def __str__(self):
        """Updating this metod"""
        return f'{self.__open_date} {self.name} сделал депозит на сумму {self.first_deposit}, с процентной ставкой {self.__percentage_charge} %'

    def withdraw_money(self, amount):
        """Withdraw money from account"""
        if amount > self.first_deposit:
            print('Не достаточно средств')
        else:
            self.first_deposit -= amount
            BankAccaunt.accounting(amount, income=False)


    def top_up_account(self, amount):
        """Top up account with a money"""
        self.first_deposit += amount
        BankAccaunt.accounting(amount, income=True)

    @staticmethod
    def slogan():
        """We create a slogan"""
        print("В левой руке — Сникерс, в правой руке — Марс. Мой пиар-менеджер — Карл Маркс. Капитал!")


    @property
    def percentage_charger(self):
        return self.__percentage_charge

    @percentage_charger.setter
    def percentage_charger(self, percentage):
        """Changing percentage charge"""
        self.__percentage_charge = percentage

    @property
    def get_todays_profit(self):
        return self.first_deposit * (self.__percentage_charge / 365)

    def money_transfer(self, other, summa):
        """I transfer money"""
        if other.first_deposit >= summa:
            self.first_deposit += summa
            other.first_deposit -= summa
        else:
            self.first_deposit += other.first_deposit
            other.first_deposit = 0

    @classmethod
    def accounting(cls, summa: int, /, *, income: bool):
        if income:
            BankAccaunt.deposit_bag += summa
        else:
            BankAccaunt.deposit_bag -= summa

    def __del__(self):
        print(f'У {self.name} закрыт счет в связи с ликвидацией банка, возвращены средсва в размере {self.first_deposit}, клиент притензий не имеет')
        BankAccaunt.accounting(self.first_deposit, income=False)
        self.first_deposit -= self.first_deposit


first_accaunt = BankAccaunt('Вадим', 1000, 20)
second_accaunt = BankAccaunt('Игорь', 2000, 15)

second_accaunt.percentage_charger = 25

first_accaunt.top_up_account(5000)

second_accaunt.withdraw_money(5500)

first_accaunt.money_transfer(second_accaunt, 3000)


