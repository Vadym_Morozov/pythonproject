# Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно.
lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for elem in lst1:
    if type(elem) == str:
        lst2.append(elem)
print(lst2)

# Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить з нього всі числа, які менше 21 і більше 74.
my_list = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 7, 8, 87, 13, 45, 67, 44]
list2 = my_list.copy()
for elem1 in list2:
    if elem1 < 21 or elem1 > 74:
        my_list.remove(elem1)
print(my_list)

# Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на 'о'.
my_str = 'Протестувати «предмет» щодо різних видів тестування. Предмет — ліфт, олівець, вікно, калькулятор тощо.'
res = my_str.split()
lst = []
for i in res:
    if i.endswith('о') or i.endswith('О'):
        lst.append(i)
    elif not i.isalpha():
        i = i[:-1]
        if i.endswith('о') or i.endswith('О'):
            lst.append(i)
        continue

print('На літеру "о" закінчується', len(lst), 'слова.')



